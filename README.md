Android Mini-App Final:TWU Grad List


The TWU Grad List mobile application runs on any device with Android software.The device that is being used to test this application is the Pixel XL API 24. The TWU Grad List app is conveniently designed in the simplest way for users to maneuver through the app. To enter a task the user must type in the task they desire, this will add the task to the list that they have made. As the user adds each task, the task will be prioritized by time/ importance. The user can input latitude and longitude by tapping on the task. Additionally, they can also edit the task name and priority when clicking on it. Once the user has completed the task, they can check it off by clicking the check box.

The process of creating the application through Android Studio came with many challenges. Learning how to work	 with the software was a task within itself but following tutorials online was helpful in learning the functionality of it. Creating the layout and finding code was another challenge that occurred. Due to this, we had to combine codes amongst different resources found online and had to make sure it still ran through and met the requirements for the project. Moreover, coding the xml files for the java classes was the biggest challenge we faced. For the first class (Main) there wasn’t any difficulty as its xml file was used to create the layout and template for the app design. However, for the other java classes, we had a hard time figuring out how to work with the code and put it in the right content in the xml files. The codes we were able to find for the xml files as reference did not work and kept giving errors after multiple attempts. The challenges that we were presented with helped us learn how to work around it and the steps to resolve each issue. In doing so it has also helped us learn how to become better application developers.
Another challenge we faced early on was pushing the files on GitLab. We created various groups and projects everytime we wanted to push the files. This was the only way we would succeed in doing so.

***************************************************************************************
        *  REFERENCES
        *  Title: <Basic Todo App Tutorial>
        *  Author: <Nathan Esquenazi>
*  Date: <N/A>
        *  Code version: <source code>
*  Availability: <https://guides.codepath.com/android/Basic-Todo-App-Tutorial#creating-our-layout >
        *
        *  Title: <meetupnamegenerator>
*  Author: <Dr. Jonathan Gratchi>
        *  Date: <N/A>
        *  Code version: <source code>
*  Availability: <https://gitlab.com/csci3313sp20/meetupnamegenerator/-/blob/master/app/src/main/java/cwit/cs/virginia/edu/cwitmeetupnamegenerator/MainActivity.javat >
        *
        *  Title: <vanshg/Bakkle>
        *  Author: <vanshg>
*  Date: <N/A>
        *  Code version: <source code>
*  Availability: https://github.com/vanshg/Bakkle/tree/master/Android/Bakkle/app/src/main/java/com/bakkle/bakkle>
        *
        *
        ***************************************************************************************/