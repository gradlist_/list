package com.e.twulist;

import java.io.Serializable;
import java.util.Date;

public class BucketItem implements Comparable<BucketItem>, Serializable{

    private String mTitle;
    private boolean mChecked;
    private String mDescription;
    private Date mDate;
    private double mLatitude;
    private double mLongitude;

    public BucketItem(String title, boolean checked, String description, Date date, double latitude, double longitude) {
        this.mTitle = title.trim();
        this.mChecked = checked;
        this.mDescription = description.trim();
        this.mDate = date;
        this.mLatitude = latitude;
        this.mLongitude = longitude;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        this.mDate = date;
    }

    public double getLatitude() { return mLatitude; }

    public void setLatitude(double d) { this.mLatitude = d; }

    public double getLongitude() { return mLongitude; }

    public void setLongitude(double d) { this.mLongitude = d; }

    @Override
    public int compareTo(BucketItem that) {
        if (this.mChecked && !that.isChecked()) {
            return 1;
        }
        else if (!this.mChecked && that.isChecked()) {
            return -1;
        }
        else {
            return this.mDate.compareTo(that.getDate());
        }
    }
}