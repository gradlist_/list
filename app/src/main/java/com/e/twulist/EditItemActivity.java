package com.e.twulist;

import android.app.usage.NetworkStats;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

public class EditItemActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private BucketItem mBucketitem;
    private ArrayList<BucketItem> mBucketList;
    private EditText editName;
    private EditText editDescription;
    private EditText editLatitude;
    private EditText editLongitude;
    private CalendarView calendar;
    private Date mDate;
    private Button saveChanges;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_item);

        mToolbar = (Toolbar) findViewById(R.id.edit_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editName = (EditText) findViewById(R.id.edit_name);
        editDescription = (EditText) findViewById(R.id.edit_description);
        editLatitude = (EditText) findViewById(R.id.edit_latitude);
        editLongitude = (EditText) findViewById(R.id.edit_longitude);
        calendar = (CalendarView) findViewById(R.id.edit_calendarView);
        saveChanges = (Button) findViewById(R.id.save_changes);

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                int actualYear = year - 1900;
                mDate = new Date(actualYear, month, dayOfMonth);
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mBucketitem = (BucketItem) extras.getSerializable("item"); //Obtaining data
            this.mBucketList = (ArrayList<BucketItem>) extras.getSerializable("bucket_list");
        }
    }

    protected void onStart() {
        super.onStart();

        editName.setText(this.mBucketitem.getTitle());
        editDescription.setText(this.mBucketitem.getDescription());
        editLatitude.setText(Double.toString(this.mBucketitem.getLatitude()));
        editLongitude.setText(Double.toString(this.mBucketitem.getLongitude()));
        calendar.setDate(this.mBucketitem.getDate().getTime());
    }

    protected void onResume() {
        super.onResume();
        saveChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = editName.getText().toString();
                String description = editDescription.getText().toString();
                String sLatitude = editLatitude.getText().toString();
                String sLongtitude = editLongitude.getText().toString();

                if (title.matches("") || description.matches("") || sLatitude.matches("") || sLongtitude.matches("")) {
                    Toast.makeText(EditItemActivity.this, "You have missing fields", Toast.LENGTH_SHORT).show();
                    return;
                }

                Double latitude = Double.parseDouble(editLatitude.getText().toString());
                Double longitude = Double.parseDouble(editLongitude.getText().toString());

                final BucketItem bucketItem = new BucketItem(title, false, description, mDate, latitude, longitude);

                Intent saveChangesIntent = new Intent(EditItemActivity.this, BucketListActivity.class);
                Log.d("EditItemActivity", "inside onClick");
                saveChangesIntent.putExtra("item", bucketItem);
                setResult(RESULT_OK, saveChangesIntent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_change_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent myIntent = new Intent(getApplicationContext(), BucketListActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
    }

    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mainIntent = new Intent(EditItemActivity.this, BucketListActivity.class);
        mainIntent.putExtra("bucket_list", mBucketList);
        startActivity(mainIntent);
    }
}