package com.e.twulist;

import android.app.usage.NetworkStats;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

public class BucketListAdapter extends RecyclerView.Adapter<BucketListAdapter.ViewHolder> {
    private static ClickListener clickListener;

    private Context mContext;
    private List<BucketItem> mBucketList;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public BucketListAdapter(Context context, List<BucketItem> bucketItems) {
        mContext = context;
        mBucketList = bucketItems;
    }

    @Override
    public BucketListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View bucketListView = inflater.inflate(R.layout.bucket_list_item, parent, false);
        return new ViewHolder(bucketListView);
    }

    @Override
    public void onBindViewHolder(final BucketListAdapter.ViewHolder viewHolder, int position) {
        final BucketItem bucketItem = mBucketList.get(position);
        viewHolder.tvTitle.setText(bucketItem.getTitle());
        viewHolder.tvDate.setText(sdf.format(bucketItem.getDate()));
        viewHolder.cbChecked.setChecked(bucketItem.isChecked());
        viewHolder.cbChecked.setTag(bucketItem);

        viewHolder.cbChecked.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                BucketItem b = (BucketItem) cb.getTag();
                b.setChecked(cb.isChecked());
                bucketItem.setChecked(cb.isChecked());
                Collections.sort(mBucketList);
                notifyItemRangeChanged(0, mBucketList.size());
                Log.i("bucketItem",bucketItem.getTitle());
            }
        });
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mBucketList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvTitle;
        public TextView tvDate;
        public CheckBox cbChecked;

        public ViewHolder(View itemView) {
            super(itemView);

            tvTitle = (TextView) itemView.findViewById(R.id.title);
            tvDate = (TextView) itemView.findViewById(R.id.date);
            cbChecked = (CheckBox) itemView.findViewById(R.id.checked);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(ClickListener clickListener) {
        BucketListAdapter.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int position, View v);
    }

    public BucketItem getBucketItemAtPos (int pos) {
        return mBucketList.get(pos);
    }
}