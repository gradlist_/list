package com.e.twulist;

import android.app.usage.NetworkStats;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class BucketListActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    ArrayList<BucketItem> mBucketList;
    private int currentPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bucket_list);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Bucket List");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getSerializable("bucket_list") != null) {
                mBucketList = (ArrayList<BucketItem>) extras.getSerializable("bucket_list");
            }
        } else {
            mBucketList = createInitialBucketList();
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.bucket_list_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new BucketListAdapter(this, mBucketList);
        addEditItemFeature((BucketListAdapter) mAdapter);
        mRecyclerView.setAdapter(mAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.add_bucket_item_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent addActivityIntent = new Intent(BucketListActivity.this, AddItemActivity.class);
                addActivityIntent.putExtra("bucket_list", mBucketList);
                startActivityForResult(addActivityIntent, 1);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyItemRemoved(currentPos);
        mAdapter.notifyItemInserted(currentPos);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bucket_list, menu);
        return true;
    }

    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static ArrayList<BucketItem> createInitialBucketList() {
        ArrayList<BucketItem> bucketList = new ArrayList<>();
        bucketList.add(new BucketItem("CS4720 homework", false, "Finish Android mini project", parseDate("2017-09-06"), 0, 0));
        bucketList.add(new BucketItem("Streak lawn", false, "Streak lawn at night", parseDate("2017-09-20"), 50, 50));
        bucketList.add(new BucketItem("Graduate", false, "Graduate in 4 years", parseDate("2018-05-20"), 300, 300));
        bucketList.add(new BucketItem("Sell car", false, "Sell car before moving", parseDate("2018-04-20"), 100, 100));
        return bucketList;
    }

    public void addEditItemFeature(BucketListAdapter adapter) {
        adapter.setOnItemClickListener(new BucketListAdapter.ClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.d("BucketListActivity", "onItemClick position: " + position);
                Intent i = new Intent(BucketListActivity.this, EditItemActivity.class);
                BucketItem item = ((BucketListAdapter)mAdapter).getBucketItemAtPos(position);
                i.putExtra("item", item);
                i.putExtra("bucket_list", mBucketList);
                currentPos = position;
                startActivityForResult(i, 0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("BucketListActivity", "onActivityResult");
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            // passedItem = data.getExtras().get("passed_item");
            Bundle extras = data.getExtras();
            if (extras != null) {
                BucketItem bucketItem = (BucketItem) extras.getSerializable("item"); //Obtaining data
                mBucketList.set(currentPos, bucketItem);
                Collections.sort(mBucketList);
                mAdapter.notifyItemRangeChanged(0, mBucketList.size());
            }
        }

        Log.d("BucketListActivity", "" + requestCode + resultCode + data.getExtras());
        if (requestCode == 1 && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                BucketItem bucketItem = (BucketItem) extras.getSerializable("item"); //Obtaining data
                Log.d("BucketListActivity", "onActivityResult finishing it");
                mBucketList.add(bucketItem);
                Collections.sort(mBucketList);
                mAdapter.notifyItemRangeChanged(0, mBucketList.size());
            }
        }
    }
}